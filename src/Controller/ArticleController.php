<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Forms;
use App\Entity\Article;
use App\Entity\Entreprise;
use App\Form\ArticleType;
use App\Repository\EntrepriseRepository;



class ArticleController extends AbstractController
{
    /**
     * @Route("/article", name="article")
     */
    public function index(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Article::class);
        
        // Retrouver tous les articles
        $articles = $repository->findAll();

        

        return $this->render('vue/article.html.twig', [
            'controller_name' => 'ArticleController',
            'title' => 'Article',
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/articleAdd", name="articleAdd")
     */
    public function add(Request $request): Response
    {
        $article = new Article();
        
        $formArt = $this->createForm(ArticleType::class, $article);
       

        $formArt->handleRequest($request);

            if ($formArt->isSubmitted() && $formArt->isValid()) {

               
                 

                $article = $formArt->getData();
                
               
                $entreprise = new Entreprise();
                $entreprise = $formArt->get('Entreprise')->getData();


                $article->setIdEntreprise($entreprise);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($article);
                $entityManager->flush();

                return $this->redirectToRoute('article');
            }

    	return $this->render('vue/articleAdd.html.twig', [
            'formArt' => $formArt->createView()
        ]);
    }


     /**
     * @Route("/articleEdit/{id}", name="articleEdit")
     */
    public function edit(Request $request, $id){
        $repository = $this->getDoctrine()->getRepository(Article::class);
        
        // Retrouver l'article par son identifiant
        $articles = $repository->find($id);

        $formArt = $this->createForm(ArticleType::class, $articles);

        $formArt->handleRequest($request);

        if ($formArt->isSubmitted() && $formArt->isValid()) {
            // va effectuer la requête d'UPDATE en base de données
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('article');
        }


        return $this->render('vue/articleAdd.html.twig',[
            'formArt' => $formArt->createView(),
            'title' => 'Modification de l\'article ',
            'articles' => $articles,
        ]);
    }
    
    /**
     * @Route("/articleDelete/{id}", name="articleDelete")
     */
    public function delete(Request $request, Article $articles){
        $em = $this->getDoctrine()->getManager();
    
        $em->remove($articles);
        $em->flush();
    
        // redirige la page
        return $this->redirectToRoute('article');
    }

}
