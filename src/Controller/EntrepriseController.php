<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Entreprise;
use App\Form\EntrepriseType;
use Symfony\Component\HttpFoundation\Request;

class EntrepriseController extends AbstractController
{
    /**
     * @Route("/entreprise", name="entreprise")
     */
    public function index(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Entreprise::class);
        
        // Retrouver tous les entreprises
        $entreprises = $repository->findAll();

        return $this->render('entreprise/index.html.twig', [
            'controller_name' => 'EntrepriseController',
            'entreprises' => $entreprises
        ]);
    }

    /**
     * @Route("/entrepriseAdd", name="entrepriseAdd")
     */
    public function add(Request $request){
        $entreprise = new Entreprise();

        $formEntreprise = $this->createForm(EntrepriseType::class, $entreprise);
       

        $formEntreprise->handleRequest($request);

            if ($formEntreprise->isSubmitted() && $formEntreprise->isValid()) {

                $entreprise = $formEntreprise->getData();


                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($entreprise);
                $entityManager->flush();

                return $this->redirectToRoute('entreprise');
            }

        return $this->render('vue/entrepriseAdd.html.twig', [
            'formEntreprise' => $formEntreprise->createView(),
            'title' => 'Création d\'une entreprise',
        ]);
    }

    /**
     * @Route("/entrepriseEdit/{id}", name="entrepriseEdit")
     */
    public function edit(Request $request, $id){
        $repository = $this->getDoctrine()->getRepository(Entreprise::class);
        
        // Retrouver l'article par son identifiant
        $entreprise = $repository->find($id);

        $formEntreprise = $this->createForm(EntrepriseType::class, $entreprise);

        $formEntreprise->handleRequest($request);

        if ($formEntreprise->isSubmitted() && $formEntreprise->isValid()) {
            // va effectuer la requête d'UPDATE en base de données
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('entreprise');
        }


        return $this->render('vue/entrepriseAdd.html.twig',[
            'formEntreprise' => $formEntreprise->createView(),
            'title' => 'Modification de l\'entrepise ',
            'entreprise' => $entreprise,
        ]);
    }
    
    /**
     * @Route("/entrepriseDelete/{id}", name="entrepriseDelete")
     */
    public function delete(Request $request, Entreprise $entreprise){
        $em = $this->getDoctrine()->getManager();
    
        $em->remove($entreprise);
        $em->flush();
    
        // redirige la page
        return $this->redirectToRoute('entreprise');
    }
}
