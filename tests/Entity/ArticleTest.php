<?php
namespace App\Tests\Entity;

use App\Entity\Article;
use PHPUnit\Framework\TestCase;

class ArticlePresent extends TestCase
{
    public function testDesignation()
    {

        $article = new Article();
        $article->setDesignation("l'oréal");
        $result = $article->getDesignation();
        $this->assertEquals("l'oréal", $result);

    }
}