<?php

namespace tests\AppBundle\Entity;

use App\Entity\Entreprise;
use PHPUnit\Framework\TestCase;


class EntrepriseTest extends TestCase {

    public function testNom(){

        $entrerpise = new Entreprise();
        $entrerpise->setNom('nom');

        $this->assertEquals('nom', $entrerpise->getNom());   
    }
    
}